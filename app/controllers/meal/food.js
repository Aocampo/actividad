import Controller from '@ember/controller';

export default Controller.extend({
	actions:{
		createFood(meal){
			let food = this.store.createRecord('food', {
				meal: meal
			})
			food.save().then(()=>{
				window.location.reload(true);
			})
		},
		deleteFood(food){
			food.destroyRecord().then(()=>{
			})
		},
		updateFood(food){
			food.save().then(()=>{
			})
		},
		onProductSelected(product){
      this.set('productSelected',product)
    },
	}
});
