import Controller from '@ember/controller';

export default Controller.extend({
  actions:{
		createProduct(){
			this.store.createRecord('product', {
				name: this.get('name'),
				calories: this.get('calories')
			}).save().then(()=>{
			})
		},
		deleteProduct(product){
			product.destroyRecord().then(()=>{
			})
		},
		updateProduct(product){
			product.save().then(()=>{
			})
		}
	}
});
