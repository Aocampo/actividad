import Controller from '@ember/controller';

export default Controller.extend({
	actions: {

		createMeal() {
				this.store.createRecord('meal', {
					name: this.get('meal')
				}).save().then(() => {
				});
			},

		editMeal(meal) {
				meal.save();
		},

		deleteMeal(meal) {
				meal.destroyRecord();
		},
		goMealDetails(meal){
			this.transitionToRoute('meal.food',meal.get('id'));
		},

		goProduct(){
			this.transitionToRoute('meal.product')
		}

	}

});
