import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
		goMeal() {
      this.transitionToRoute('meal')
    }
	}
});
