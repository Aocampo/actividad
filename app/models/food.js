import DS from 'ember-data';
import { computed } from '@ember/object';
import { isBlank } from '@ember/utils';

export default DS.Model.extend({
  name: DS.attr('string'),
  calories: DS.attr('number'),
  meal: DS.belongsTo('meal'),
  product: DS.belongsTo('product')
});
