import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | meal/food', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:meal/food');
    assert.ok(controller);
  });
});
