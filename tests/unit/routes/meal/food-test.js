import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | meal/food', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:meal/food');
    assert.ok(route);
  });
});
